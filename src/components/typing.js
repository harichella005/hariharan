//Required library for typing text
import React from 'react'
import '../App.css';
import Typewriter from "typewriter-effect";


//Main funtion that holds the div tag instide that div tag the auto type text will stored 
function Autotyperole() {
    return (
        <div className='typing'>
          {/* Typewriter variable to perform the auto typing text */}
              <Typewriter 
              //Holds all the text stings, to start automatic or not then loop it or not 
                options={{
                  strings: ['Web Developer', 'GUI/CLI Designer', 'Ethical Hacker', 'WebApps Pentester', 'Full stack developer' ],
                  autoStart: true,
                  loop: true,
                  
                }}
                />
        </div>
    )
}

export default Autotyperole
