//Libraries required to run the header 
import "../App.css"
//import Hp from "../assests/bg4.jpg"
import Hari from "../assests/hari.png"
import Ham from "../assests/ham.png";
import { useEffect } from "react";
import $ from 'jquery';

//main function that holds the header elements
const App = () => {
  /* The useEffect Hook allows you to perform side effects in your components. 
  This helps to lock the header component when the biosection reaches the 280px */
    useEffect(() => {
        $(window).scroll(function(){
          if ($(this).scrollTop() > 340) {
            $('.navbar-sec').addClass('fixed');
          } else {
            $('.navbar-sec').removeClass('fixed');
          }
        });
      }, []);
    //Header component elements that holds all the <a> tag in the header and logo them hamburger toggle for mobile
    return (
        <div className="navbar-sec header-sec">
          <img className="header-pic" src={Hari} alt="header-logo"></img>
          <div className='ham'>
            <img className='hamburger' src={Ham} alt='ham-menu'></img>
            <div className='hammenu'>
              <nav className='list'>
                <a class="hbutton" href='http://google.com' target="_parent" width="100px" height="300px">Home</a>
                <a class="hbutton" href='#section'>About</a>
                <a class="hbutton" href='http://google.com'>What I Do</a>
                <a class="hbutton" href='http://google.com'>Resume</a>
                <a class="hbutton" href='http://google.com'>Portfolio</a>
                <a class="hbutton" href='http://google.com'>Contact</a>
                <a class="hbutton" href='http://google.com'>Blog</a>
              </nav>
            </div>
          </div>
        </div>
    )
};
export default App;