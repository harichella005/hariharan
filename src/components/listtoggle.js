import "../App.css"
import React, { useState } from "react";

const App = () => {
    const [toggle, setToggle] = useState(false)
    return (
        <>
        {/* <button 
            onClick={() => setToggle(!toggle)} 
            class="btn btn-primary mb-5">
                Toggle State
        </button> */}
        <button onClick={() => setToggle(!toggle)} class="tog purple darken-3"></button>
        {toggle && (
            <ul class="list-group">
                <a href="https://google.com" class="list-group-item">Home</a>
                <li class="list-group-item">contact</li>
                <li class="list-group-item">blog</li>
                <li class="list-group-item">service</li>
            </ul>
        )}
        </>
    )
};
export default App;