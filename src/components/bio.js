import React from 'react'
import { useEffect } from "react";
import '../App.css';
import Aos from "aos";
import "aos/dist/aos.css";
import { MouseParallaxContainer, MouseParallaxChild } from "react-parallax-mouse";


function App() {
    useEffect(() => {
        Aos.refresh();
        Aos.init({ duration: 1000, 
        easing: 'ease-in-sine',
        once: true,
        mirror: false});
      }, []);
    return (
        <>
        <section className="bio-sec">
            <div className="high-bio" id="bio" data-aos="flip-right">
                {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span className='start'>H</span> */}
                <p className='start' data-aos="fade-right">Hello, Myself Hariharan C and im a 22 year old Computer 
                science engineer who fasinated about programming and have keen knowledge in it, 
                after understanding the indepth knowledge of computer and technology. 
                While googling about the programming i came to know a word Bug Hunting, 
                Yes the word itself feels something intresting and before braking the system we 
                have to know about the computer system and how the technology is working.</p>
                <p data-aos="fade-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the
                1500s, when an unknown printer took a galley of type and scrambled it to
                make a type specimen book. It has survived not only five centuries, but
                also the leap into electronic typesetting, remaining essentially
                unchanged. It was popularised in the 1960s with the release of Letraset
                sheets containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
            <div data-aos="flip-right" className='profile-sec'>
                <MouseParallaxContainer>
                    <MouseParallaxChild
                        factorX={0.03}
                        factorY={0.03}
                        updateStyles={{
                            filter: "invert(1)",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            width: "auto",
                            height: "5vh"
                        }}
                    >
                        <img id="inner" src="https://raw.githubusercontent.com/hariharan005/hariharan/main/hari.png" className="profile" alt="My profile pic"/>
                    </MouseParallaxChild>
                </MouseParallaxContainer>
            </div>
        </section>
        </>
    )
}

export default App
