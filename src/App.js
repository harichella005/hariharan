//Required library for the App page. Main page content that will reflect in web page in home directory
//merging new
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import Typing from "./components/typing";
import Header from "./components/header";
import Footer from "./components/footer";
import Bio from "./components/bio"
import Program from "./components/programming-language"
import Frontend from "./components/frontend"
import Backend from "./components/backend"//hello

//Here is the main function created to run this application contains all the components 
function App() {
  return (
    //Div tag to hold all the components
    <div className="App">
      {/* placed all the components  */}
      <header className="App-header">
        <div className="header-sec"></div>
        <Header></Header>
        <Typing></Typing>
        <Bio></Bio>
        <Program></Program>
        <Frontend></Frontend>
        <Backend></Backend>
        <Footer></Footer>
      </header>
    </div>
  );
}

export default App;
